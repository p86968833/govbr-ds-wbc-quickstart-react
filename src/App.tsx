import "@govbr-ds/core/dist/core.min.css";
import "@govbr-ds/webcomponents/dist/webcomponents.common.js";
import { BrowserRouter as Router } from "react-router-dom";
import Breadcrumb from "./components/Breadcrumb/Breadcrumb";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import Menu from "./components/Menu/Menu";

const App = () => {
  return (
    <Router>
      <header>
        <Header />
      </header>
      <main className="d-flex flex-fill mb-5" id="main">
        <div className="container-lg d-flex">
          <div className="row">
            <menu className="col-sm-4 col-lg-2">
              <Menu />
            </menu>
            <div className="col mb-5">
              <Breadcrumb></Breadcrumb>
              <div className="main-content pl-sm-3 mt-4" id="main-content">
                <h1>Lorem ipsum</h1>
                <br-message state="warning" title="Atenção." show-icon>
                  Esse projeto NÃO ESTÁ PRONTO PARA PRODUÇÃO! Sinta-se à vontade
                  para usá-lo como ponto de partida para seu projeto ou como
                  exemplo de uso, mas tenha em mente que configurações,
                  melhorias e correções precisarão ser feitos.
                  <hr />
                  Para mais informações acesse o{" "}
                  <a href="https://gov.br/ds" target="_blank">
                    nosso site
                  </a>
                  .
                </br-message>
                <div>
                  <p>
                    Blanditiis, qui? Obcaecati consequuntur delectus pariatur
                    beatae assumenda autem reprehenderit adipisci voluptatum
                    exercitationem quod laborum, tempore corporis id officiis
                    optio, animi blanditiis labore aspernatur illum, molestiae
                    natus ipsa? Quo, sapiente.
                  </p>
                  <p>
                    Adipisci illo hic ratione dolores minus sit odio ipsum sequi
                    iste tenetur, cupiditate fuga asperiores sed temporibus
                    corrupti autem iusto architecto nemo veritatis eum.
                    Aspernatur earum voluptates alias explicabo ut?
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
      <footer>
        <Footer />
      </footer>
    </Router>
  );
};

export default App;
